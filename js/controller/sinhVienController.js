function getValueForm() {
  var maSV = document.getElementById("txtMaSV").value;
  var tenSV = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemLy = document.getElementById("txtDiemLy").value * 1;
  var diemHoa = document.getElementById("txtDiemHoa").value * 1;

  var sinhVien = new SinhVien(maSV, tenSV, email, diemToan, diemLy, diemHoa);
  return sinhVien;
}

function viewTable(listSV) {
  var content = "";
  listSV.forEach((e, i) => {
    content += `<tr>
      <td>${e.maSV}</td>
      <td>${e.tenSV}</td>
      <td>${e.email}</td>
      <td>${e.diemTB()}</td>
      <td>
        <button class="btn btn-info my-1 mx-1" onclick="edit('${e.maSV}')">
          Sửa
        </button>
        <button
          onclick="deleteSV('${e.maSV}')"
          class="btn btn-danger my-1 mx-1"
        >
          Xoá
        </button>
      </td>
    </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = content;
}

function edit(id) {
  var i = findI(id, listSV);

  if (i == -1) {
    return;
  }

  document.getElementById("txtMaSV").value = listSV[i].maSV;
  document.getElementById("txtTenSV").value = listSV[i].tenSV;
  document.getElementById("txtEmail").value = listSV[i].email;
  document.getElementById("txtDiemToan").value = listSV[i].diemToan;
  document.getElementById("txtDiemLy").value = listSV[i].diemLy;
  document.getElementById("txtDiemHoa").value = listSV[i].diemHoa;
}

function deleteSV(id) {
  var i = findI(id, listSV);

  if (i == -1) {
    return;
  }
  console.log("Xoá", listSV[i]);
  listSV.splice(i, 1);
  viewTable(listSV);
  saveInStorage();
}

function update() {
  var updateSV = getValueForm();

  var index = findI(updateSV.maSV, listSV);
  if (!isValid()) {
    return;
  }
  if (index !== -1) {
    listSV[index] = updateSV;
    viewTable(listSV);
    saveInStorage();
    document.getElementById("spanMaSV").innerHTML = ``;
  } else {
    document.getElementById(
      "spanMaSV"
    ).innerHTML = `Mã sinh viên không tồn tại`;
  }
}

function saveInStorage() {
  // covert to json
  var listSVJson = JSON.stringify(listSV);
  console.log(listSVJson);
  // save in storage
  localStorage.setItem(LISTSV_LOCALSTORAGE, listSVJson);
}

function isValid() {
  var isValidMSV = validatorSV.kiemTraRong(
    "txtMaSV",
    "spanMaSV",
    "Mã sinh viên không được để rỗng"
  );
  var isValidMail = validatorSV.kiemTraMail("txtEmail", "spanEmailSV");
  var isValidName = validatorSV.kiemTraTen("txtTenSV", "spanTenSV");
  return (
    isValidMSV &
    isValidMail &
    isValidName &
    validatorSV.kiemTraSo("txtDiemToan", "spanToan") &
    validatorSV.kiemTraSo("txtDiemLy", "spanLy") &
    validatorSV.kiemTraSo("txtDiemHoa", "spanHoa")
  );
}

function findI(id, array) {
  return array.findIndex((e) => {
    return e.maSV == id;
  });
}
