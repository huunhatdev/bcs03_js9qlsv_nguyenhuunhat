function ValidatorSV() {
  this.kiemTraRong = function (idTarget, idError, msgError) {
    var valueTarget = document.getElementById(idTarget).value.trim();
    if (valueTarget == "") {
      document.getElementById(idError).innerHTML = msgError;
      return false;
    } else {
      document.getElementById(idError).innerHTML = "";
      return true;
    }
  };

  this.checkIDContain = function (newSinhVien, listSV) {
    var index = listSV.findIndex((item) => {
      return item.maSV == newSinhVien.maSV;
    });
    if (index == -1) {
      document.getElementById("spanMaSV").innerHTML = ``;
      return true;
    }
    document.getElementById(
      "spanMaSV"
    ).innerHTML = `Mã sinh viên không được trùng`;
    return false;
  };

  this.kiemTraMail = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value.trim();
    let pattern = /^[\w]+([\.-]?[\w]+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$$/;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerHTML = ``;
      return true;
    }
    document.getElementById(idError).innerHTML = `Email không hợp lệ`;
    return false;
  };

  this.kiemTraTen = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value.trim();
    let pattern =
      /^([A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+)((\s{1}[A-VXYỲỌÁẦẢẤỜỄÀẠẰỆẾÝỘẬỐŨỨĨÕÚỮỊỖÌỀỂẨỚẶÒÙỒỢÃỤỦÍỸẮẪỰỈỎỪỶỞÓÉỬỴẲẸÈẼỔẴẺỠƠÔƯĂÊÂĐa-vxyỳọáầảấờễàạằệếýộậốũứĩõúữịỗìềểẩớặòùồợãụủíỹắẫựỉỏừỷởóéửỵẳẹèẽổẵẻỡơôưăêâđ]+){1,})$/;
    if (pattern.test(valueInput)) {
      document.getElementById(idError).innerHTML = ``;
      return true;
    }
    document.getElementById(idError).innerHTML = `Tên không hợp lệ`;
    return false;
  };

  this.kiemTraSo = function (idTarget, idError) {
    let valueInput = document.getElementById(idTarget).value.trim();
    let pattern = /^\d{1,2}$/;
    if (
      pattern.test(valueInput) &&
      valueInput * 1 <= 10 &&
      valueInput * 1 >= 0
    ) {
      document.getElementById(idError).innerHTML = ``;
      return true;
    }
    document.getElementById(idError).innerHTML = `Điểm nhập không hợp lệ`;
    return false;
  };
}
