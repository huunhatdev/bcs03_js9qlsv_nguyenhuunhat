var SinhVien = function (_maSV, _tenSV, _email, _diemToan, _diemLi, _diemHoa) {
  this.maSV = _maSV;
  this.tenSV = _tenSV;
  this.email = _email;
  this.diemToan = _diemToan;
  this.diemLy = _diemLi;
  this.diemHoa = _diemHoa;
  this.diemTB = function () {
    return ((this.diemToan + this.diemLy + this.diemHoa) / 3).toFixed(1);
  };
  // this.diemTb = "10";
};

// export default SinhVien;
