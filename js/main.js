// import SinhVien from "./model/sinhVienModel";

var listSV = [];
var validatorSV = new ValidatorSV();

//get data in storage
const LISTSV_LOCALSTORAGE = "LISTSV_LOCALSTORAGE";
var getListSVJson = localStorage.getItem(LISTSV_LOCALSTORAGE);
if (getListSVJson) {
  listSV = JSON.parse(getListSVJson);
  listSV = listSV.map(function (item) {
    return new SinhVien(
      item.maSV,
      item.tenSV,
      item.email,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  console.log(listSV);
  viewTable(listSV);
}

function themSinhVien() {
  var newSinhVien = getValueForm();

  // var index = listSV.findIndex((item) => {
  //   return item.maSV == newSinhVien.maSV;
  // });
  var checkID = validatorSV.checkIDContain(newSinhVien, listSV);

  // return;

  if (isValid() && checkID) {
    listSV.push(newSinhVien);

    console.log(listSV);
    saveInStorage();
  }

  viewTable(listSV);
}

function searchSV() {
  var arrSearch = [];
  var nameVal = document.getElementById("txtSearch").value.trim();
  if (nameVal) {
    for (let i = 0; i < listSV.length; i++) {
      console.log(listSV[i].tenSV);
      if (listSV[i].tenSV.includes(nameVal)) {
        arrSearch.push(listSV[i]);
      }
    }
    viewTable(arrSearch);
  } else {
    viewTable(listSV);
  }
}
